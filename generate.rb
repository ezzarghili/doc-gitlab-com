#!/usr/bin/ruby
# Generates the html documentation from the markdown files in the GitLab repo.
# Call with no-clone to skip the timeconsuming closing process.
# By default, the site is built in /srv/doc-gitlab-com. This location can be
# overridden with the SITE_PATH environment variable.
#
# By default, this script will not create console output. If you want to see
# progress indicators, set the `PROGRESS` environment variable:
#
#   PROGRESS=1 ./generate.rb
#

EXCLUDE_PATHS = %w{install/packages.md}

def main
  $progress.puts 'Updating doc.gitlab.com'

  require 'fileutils'
  require 'find'

  repos = {
    'ce' => 'https://gitlab.com/gitlab-org/gitlab-ce.git',
    'ee' => 'https://gitlab.com/gitlab-org/gitlab-ee.git',
    'omnibus' => 'https://gitlab.com/gitlab-org/omnibus-gitlab.git'
  }
  repo_path = File.expand_path('..', __FILE__)
  site_path = ENV['SITE_PATH'] || '/srv/doc-gitlab-com'

  repos.each do |relative_path, clone_url|

    tmp_dir = File.join('/tmp/gitlab', relative_path)

    unless 'no-clone' == ARGV[0]
      system("rm -rf #{tmp_dir}")
      system("git clone #{ENV['PROGRESS'] ? nil : '--quiet'} -- #{clone_url} #{tmp_dir}")
    end

    $progress.puts 'Deleting files that should be excluded'
    EXCLUDE_PATHS.each do |excluded_path|
      delete_command = %W(find #{tmp_dir} -path *#{excluded_path} -delete)
      delete_command << '-print' if ENV['PROGRESS']
      system(*delete_command)
    end

    doc_root = File.join(tmp_dir, 'doc')
    doc_directories = Find.find(doc_root).map do |path|
      File.dirname(path.sub(doc_root, ''))
    end.uniq

    all_directories = doc_directories.unshift('') # Also add the root README.md, must be the first one.

    $progress.print "Generating pages for #{relative_path}: "
    all_directories.each do |directory|
      destination_dir = [site_path, relative_path, directory].join('/')
      FileUtils.rm_rf(destination_dir) if File.exist?(destination_dir)
      FileUtils.mkdir_p(destination_dir)
      path = File.join(tmp_dir, 'doc', directory, "*.md")

      Dir[path].each do |markdown_file|
        template_path = File.join(repo_path, 'template.html')
        # Because the README files are like tables of contents, don't add
        # another table of contents to them.
        toc = markdown_file.include?('README') ? nil : '--toc'
        html = `pandoc #{toc} --template #{template_path} --from markdown_github-hard_line_breaks #{markdown_file}`

        html.gsub!(/href="(\S*)"/) do |result| # Fetch all links in the HTML Document
          if /http/.match(result).nil? # Check if link is internal
            result.gsub!(/\.md/, '.html') # Replace the extension if link is internal
          end
          result
        end
        repo_link = ''
        if clone_url.include? '@'
          repo_link = clone_url.gsub('.git', '').gsub(':','/').gsub('git@', 'https://')
        else
          repo_link = clone_url.gsub('.git', '')
        end
        filename = File.basename(markdown_file)
        html = html.gsub(/DOC_LINK/, "#{repo_link}/blob/master/doc/#{directory}/#{filename}")

        html_filename = filename.gsub('.md', '.html')
        File.open(File.join(site_path, relative_path, directory, html_filename), 'w') {
          |file| file.write(html)
        }

        $progress.print '.'
      end
    end
    $progress.puts '' # Create a newline

    $progress.print "Copying png images for #{relative_path}: "
    all_directories.each do |directory|
      destination_dir = [site_path, relative_path, directory].join('/')
      path = File.join(tmp_dir, 'doc', directory, "*.png")

      Dir[path].each do |src_image|
        image_basename = File.basename(src_image)
        dest_image = File.join(site_path, relative_path, directory, image_basename)
        FileUtils.copy_file(src_image, dest_image)
        $progress.print '.'
      end
    end
    $progress.puts '' # Create a newline
  end

  $progress.puts 'Generating stylesheets'
  repo_stylesheets_path = File.join(repo_path, 'stylesheets')
  site_stylesheets_path = File.join(site_path, 'stylesheets')
  FileUtils.rm_rf(site_stylesheets_path) if File.exist?(site_stylesheets_path)
  FileUtils.cp_r(repo_stylesheets_path, site_stylesheets_path)

  $progress.puts 'Generating favicon'
  repo_favicon_path = File.join(repo_path, 'favicon.ico')
  site_favicon_path = File.join(site_path, 'favicon.ico')
  File.delete(site_favicon_path) if File.exist?(site_favicon_path)
  FileUtils.cp(repo_favicon_path, site_favicon_path)

  $progress.puts 'Generating index'
  repo_index_path = File.join(repo_path, 'index.html')
  site_index_path = File.join(site_path, 'index.html')
  File.delete(site_index_path) if File.exist?(site_index_path)
  FileUtils.cp(repo_index_path, site_index_path)

  $progress.puts 'Generating 404.html'
  repo_404_path = File.join(repo_path, '404.html')
  site_404_path = File.join(site_path, '404.html')
  File.delete(site_404_path) if File.exist?(site_404_path)
  FileUtils.cp(repo_404_path, site_404_path)

  $progress.puts 'Done'
end

if ENV['PROGRESS']
  $progress = $stdout
else
  require 'stringio'
  $progress = StringIO.new
end

main
